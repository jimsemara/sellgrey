function SellGreyItems()
    TotalPrice = 0
    for myBags = 0,4 do
        for bagSlots = 1, C_Container.GetContainerNumSlots(myBags) do
            CurrentItemLink = C_Container.GetContainerItemLink(myBags, bagSlots)
                if CurrentItemLink then
                    _, _, itemQuality, _, _, _, _, _, _, _, itemSellPrice = GetItemInfo(CurrentItemLink)
                    currentItemInfo = C_Container.GetContainerItemInfo(myBags, bagSlots)
                    itemCount = currentItemInfo.stackCount
                    if itemQuality == 0 and itemSellPrice ~= 0 then
                        TotalPrice = TotalPrice + (itemSellPrice * itemCount)
                        print("Sold: ".. CurrentItemLink .. " for " .. GetCoinTextureString(itemSellPrice * itemCount))
                        C_Container.UseContainerItem(myBags, bagSlots)
                    end
                end
        end
    end
    if TotalPrice ~= 0 then
        print("Total Price for all items: " .. GetCoinTextureString(TotalPrice))
    else
        print("No items were sold.")
    end
end

local BtnSellGrey = CreateFrame( "Button" , "SellGreyBtn" , MerchantFrame, "UIPanelButtonTemplate" )
BtnSellGrey:SetText("Sell Grey")
BtnSellGrey:SetWidth(90)
BtnSellGrey:SetHeight(21)
BtnSellGrey:SetPoint("TopRight", -180, -30 )
BtnSellGrey:RegisterForClicks("AnyUp")
BtnSellGrey:SetScript("Onclick", SellGreyItems)
