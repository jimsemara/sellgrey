# sellgrey

## Authors, credit and acknowledgment
The author of this project is GreyFoxMagic  
All rights reserved by GreyFoxMagic  
https://www.curseforge.com/members/greyfoxmagic/projects

## Description
I was using this addon for years now and since the newest patch and API changes broke the functionality,  
I was eager to fix it :)
